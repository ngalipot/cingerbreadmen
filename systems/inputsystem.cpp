#include "inputsystem.hpp"
#include "events/events.hpp"
#include "conf.hpp"
#include <SDL.h>

InputSystem::InputSystem() : exit(false)
{
}

auto InputSystem::update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void {
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type) {
        case SDL_QUIT:
            exit = true;
            events.emit<EscapeOrdered>();
            break;
        case SDL_MOUSEBUTTONDOWN:
            if (event.button.button == SDL_BUTTON_LEFT) {
                events.emit<TargetSelected>(Position(event.button.x / TILE_SIZE,
                                                     event.button.y / TILE_SIZE));
            } else if (event.button.button == SDL_BUTTON_RIGHT) {
                events.emit<MovementOrdered>(Position(event.button.x / TILE_SIZE,
                                                      event.button.y / TILE_SIZE));
            }
            break;
        }
    }
}

auto InputSystem::emergencyExit() -> bool
{
    return exit;
}
