#ifndef INPUTSYSTEM_HPP
#define INPUTSYSTEM_HPP

#include "entityx/entityx.h"

class InputSystem : public entityx::System<InputSystem> {
public:
    InputSystem();
    auto update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void override;
    auto emergencyExit() -> bool;
private:
    bool exit;
};

#endif // INPUTSYSTEM_HPP
