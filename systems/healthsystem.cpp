#include "healthsystem.hpp"
#include "components/health.hpp"
#include "components/position.hpp"
#include <iostream>

using namespace std;

HealthSystem::HealthSystem(GameStage *gameStage) : stage(gameStage)
{

}

auto HealthSystem::update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void {
    Health::Handle health;
    Position::Handle position;
    for (auto entity : es.entities_with_components(position, health)) {
        if (stage->tileIdAt(position->getX(), position->getY()) == 5) {
            health->setHealth(health->getHealth() - 1);
        }
        if (health->getHealth() <= 0) {
            entity.destroy();
        }
    }
}
