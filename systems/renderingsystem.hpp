#ifndef RENDERINGSYSTEM_HPP
#define RENDERINGSYSTEM_HPP

#include "core/gamestage.hpp"
#include "entityx/entityx.h"
#include "components/position.hpp"
#include "components/rendering.hpp"
#include <SDL.h>
#include <memory>
#include <iostream>

struct Window;
struct Renderer;
struct Texture;
struct Surface;

struct Window {
    struct Deleter {
        auto operator()(SDL_Window *win) -> void {
            SDL_DestroyWindow(win);
        }
    };
    using Ptr = std::unique_ptr<SDL_Window, Deleter>;
    Window(const std::string &title, int x, int y, int width, int height, Uint32 flags) :
        win(Ptr(SDL_CreateWindow(title.c_str(), x, y, width, height, flags))) {
    }
    Ptr win;
};

struct Texture {
    struct Deleter {
        auto operator()(SDL_Texture *tex) -> void {
            SDL_DestroyTexture(tex);
        }
    };
    using Ptr = std::unique_ptr<SDL_Texture, Deleter>;
    Texture() : tex(nullptr) {}
    auto loadFromFile(const std::string &fileName, Renderer &renderer) -> bool;
    auto createFromSurface(Surface &surface, Renderer &renderer) -> bool;
    Ptr tex;
};

struct Surface {
    struct Deleter {
        auto operator()(SDL_Surface *surf) -> void {
            SDL_FreeSurface(surf);
        }
    };
    using Ptr = std::unique_ptr<SDL_Surface, Deleter>;
    Surface(int width, int height);
    Surface(std::string fileName);
    auto blit(Surface &source, SDL_Rect &clip, SDL_Rect &pos) -> int {
        if (SDL_BlitSurface(source.surf.get(), &clip, surf.get(), &pos) < 0) {
            std::cerr << "SDL: " << SDL_GetError() << std::endl;
        }
    }
    Ptr surf;
};

struct Renderer {
    struct Deleter {
        auto operator()(SDL_Renderer *ren) -> void {
            SDL_DestroyRenderer(ren);
        }
    };
    using Ptr = std::unique_ptr<SDL_Renderer, Deleter>;
    Renderer(Window &window) :
        ren(SDL_CreateRenderer(window.win.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)) {}
    auto clear() -> void {
        SDL_RenderClear(ren.get());
    }
    auto copy(Texture &texture, SDL_Rect &clip, SDL_Rect &pos) -> void {
        SDL_RenderCopy(ren.get(), texture.tex.get(), &clip, &pos);
    }
    auto present() -> void {
        SDL_RenderPresent(ren.get());
    }
    Ptr ren;
};

class RenderingSystem : public entityx::System<RenderingSystem> {
public:
    RenderingSystem(GameStage *stage);
    auto update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void override;
private:
    auto drawBackground() -> void;
    auto drawEntity(entityx::Entity &entity) -> void;
    Window window;
    GameStage *stage; // back pointer (weak)
    Position::Handle position;
    Rendering::Handle rendering;
    Renderer renderer;
    Texture background;
    Texture shapes[5];
};

#endif // RENDERINGSYSTEM_HPP
