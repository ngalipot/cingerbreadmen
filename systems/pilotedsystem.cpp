#include "pilotedsystem.hpp"

#include "ai/pathfinding.hpp"
#include "components/position.hpp"
#include "components/movement.hpp"
#include "components/piloted.hpp"
#include <iostream>

using namespace pathfinding;
using namespace std;

PilotedSystem::PilotedSystem(GameStage *gameStage) :
    stage(gameStage),
    orderPosition(0, 0) {
    for (auto i = 0; i < GBM_STATE_LAST; i++) {
        was[i] = false;
    }
}

auto PilotedSystem::configure(entityx::EventManager &event_manager) -> void {
    event_manager.subscribe<TargetSelected>(*this);
    event_manager.subscribe<MovementOrdered>(*this);
}

auto PilotedSystem::update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void {
    Position::Handle position;
    Movement::Handle movement;
    Piloted::Handle piloted;
    for (auto entity : es.entities_with_components(position, movement, piloted)) {
        if (was[GBM_TARGET_SELECTED]) {
            piloted->setSelected(position->operator ==(orderPosition));
        } // We don't take redundant orders into account.
        if (was[GBM_MOVEMENT_ORDERED] && (!movement->hasNextMove() || orderPosition != movement->getTarget())) {
            if (piloted->isSelected()) {
                Position pos = *position.get();
                auto path = shortestPathBetween(pos, orderPosition, manhattanDistance, [&](const Position &p) -> bool {
                    p.getX() >= 0 &&
                    p.getY() >= 0 &&
                    p.getX() < stage->width() &&
                    p.getY() < stage->height() &&
                    stage->isWalkableAt(int(movement->getType()), p.getX(), p.getY());
                });
                path.push_back(pos);
                movement->setMoves(path);
            }
        }
    }
    for (auto i = 0; i < GBM_STATE_LAST; i++) {
        was[i] = false;
    }
}

auto PilotedSystem::receive(const TargetSelected &targetSelected) -> void {
    was[GBM_TARGET_SELECTED] = true;
    orderPosition = targetSelected.getTarget();
}

auto PilotedSystem::receive(const MovementOrdered &movementOrdered) -> void {
    was[GBM_MOVEMENT_ORDERED] = true;
    orderPosition = movementOrdered.getGoal();
}
