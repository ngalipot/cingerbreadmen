#ifndef HEALTHSYSTEM_H
#define HEALTHSYSTEM_H

#include "entityx/entityx.h"
#include "core/gamestage.hpp"

class HealthSystem : public entityx::System<HealthSystem> {
public:
    HealthSystem(GameStage *stage);
    auto update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void override;
private:
    GameStage *stage; // back pointer (weak)
};

#endif // HEALTHSYSTEM_H
