#ifndef __CONF_HPP__
#define __CONF_HPP__

#include <cstddef>

const int TILE_SIZE{32};
const int HEALTHBAR_HEIGHT{5};
const int HEALTHBAR_OFFSET{8};

#endif // __CONF_HPP__
