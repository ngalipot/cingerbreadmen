#ifndef PILOTEDSYSTEM_HPP
#define PILOTEDSYSTEM_HPP

#include "components/position.hpp"
#include "core/gamestage.hpp"
#include "entityx/entityx.h"
#include "events/events.hpp"

enum PilotedStates { GBM_MOVEMENT_ORDERED, GBM_TARGET_SELECTED, GBM_STATE_LAST };

class PilotedSystem : public entityx::System<PilotedSystem>, public entityx::Receiver<PilotedSystem> {
public:
    PilotedSystem(GameStage *stage);
    auto update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void override;
    auto configure(entityx::EventManager &event_manager) -> void override;
    auto receive(const TargetSelected &targetSelected) -> void;
    auto receive(const MovementOrdered &movementOrdered) -> void;
private:
    GameStage *stage; // back pointer (weak)
    Position orderPosition;
    bool was[GBM_STATE_LAST];
};

#endif // PILOTEDSYSTEM_HPP
