#include "movementsystem.hpp"
#include "components/position.hpp"
#include "components/movement.hpp"
#include <algorithm>

using namespace std;

static vector<int> obstacles[] = {{3, 4}, {4}, {}}; // EARTH, WATER, AIR

MovementSystem::MovementSystem(GameStage *gameStage) :
    stage(gameStage)
{
    for (int i = 0; i < 3; i++) {
        for (auto x = 0; x < stage->width(); x++) {
            for (auto y = 0; y < stage->height(); y++) {
                auto id = int(stage->tileIdAt(x, y));
                stage->setWalkableAt(i, x, y, find(begin(obstacles[i]), end(obstacles[i]), id) == end(obstacles[i]));
            }
        }
    }
}

auto MovementSystem::update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void {
    Position::Handle position;
    Movement::Handle movement;
    for (auto entity : es.entities_with_components(position, movement)) {
        movement->moveDuring(dt);
        if (movement->hasNextMove()) {
            position->set(movement->nextMove());
        }
    }
}
