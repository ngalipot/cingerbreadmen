#include "renderingsystem.hpp"

#include <SDL_image.h>
#include "components/movement.hpp"
#include "components/position.hpp"
#include "components/health.hpp"
#include "components/rendering.hpp"
#include "components/piloted.hpp"
#include "conf.hpp"

using namespace std;

//#define GBM_RENDERING_INTERPOLATE

Surface::Surface(int width, int height) {
    surf = Surface::Ptr(SDL_CreateRGBSurface(0, width, height, 32,
                                            0, 0, 0, 0));
    if (surf.get() == 0) {
        cerr << "SDL: " << SDL_GetError() << endl;
    }
}

Surface::Surface(std::string fileName) {
    surf = Surface::Ptr(IMG_Load(fileName.c_str()));
    if (surf.get() == 0) {
        cerr << "SDL: " << SDL_GetError() << endl;
    } else {
        auto key = SDL_MapRGB(surf.get()->format, 255, 255, 255);
        SDL_SetColorKey(surf.get(), SDL_TRUE, key);
    }
}

auto Texture::createFromSurface(Surface &surface, Renderer &renderer) -> bool {
    tex = Texture::Ptr(SDL_CreateTextureFromSurface(renderer.ren.get(), surface.surf.get()));
    if (tex.get() == 0) {
        cerr << "SDL: " << SDL_GetError() << endl;
    }
    return true;
}

enum Images { TILESET, SELECTION, CURSOR, GHOST, LIVES, LAST_IMG };
static const char* fileNames[] = {
    "img/simple-tileset.png",
    "img/selected.png",
    "img/cursor.png",
    "img/ghost.png",
    "img/lives.png"
};

RenderingSystem::RenderingSystem(GameStage *gameStage) :
    window("Gnurtz", 0, 0, 800, 600, SDL_WINDOW_SHOWN),
    stage(gameStage),
    renderer(window)
{
    drawBackground();
    for (int i = TILESET; i < LAST_IMG; i++) {
        Surface img(fileNames[i]);
        shapes[i].createFromSurface(img, renderer);
    }
}

auto RenderingSystem::drawBackground() -> void {
    Surface tilesetImg(fileNames[TILESET]);
    Surface bgSurface(800, 600);
    SDL_Rect clip{0, 0, TILE_SIZE, TILE_SIZE}, pos{0, 0, TILE_SIZE, TILE_SIZE};
    for (auto x = 0; x < stage->width(); x++) {
        for (auto y = 0; y < stage->height(); y++) {
            auto id = stage->tileIdAt(x, y) - 1;
            pos.x = x * TILE_SIZE;
            pos.y = y * TILE_SIZE;
            clip.x = id * TILE_SIZE;
            bgSurface.blit(tilesetImg, clip, pos);
        }
    }
    background.createFromSurface(bgSurface, renderer);
}

static int interpolate(int a, int b, double delta) {
        return (int) (a * (1.0 - delta) + b * delta);
}

auto RenderingSystem::drawEntity(entityx::Entity &entity) -> void {
    SDL_Rect pos{TILE_SIZE * position->getX(), TILE_SIZE * position->getY(), TILE_SIZE, TILE_SIZE};
    SDL_Rect clip{0, 0, TILE_SIZE, TILE_SIZE};
#ifdef GBM_RENDERING_INTERPOLATE
    if (entity.has_component<Movement>()) {
        auto movement = entity.component<Movement>();
        if (movement->hasNextNextMove()) {
            auto nextMove = movement->nextNextMove();
            auto delta = movement->getDelta();
            pos.x = interpolate(pos.x, TILE_SIZE * nextMove.getX(), delta);
            pos.y = interpolate(pos.y, TILE_SIZE * nextMove.getY(), delta);
        }
    }
#endif
    renderer.copy(shapes[GHOST], clip, pos);
    if (entity.has_component<Piloted>() && entity.component<Piloted>()->isSelected()) {
        renderer.copy(shapes[SELECTION], clip, pos);
        if (entity.has_component<Health>()) {
            auto health = entity.component<Health>();
            pos.y -= HEALTHBAR_OFFSET;
            pos.h = HEALTHBAR_HEIGHT;
            clip.x = TILE_SIZE * (((health->getMaxHealth() - health->getHealth()) * TILE_SIZE) / health->getMaxHealth());
            renderer.copy(shapes[LIVES], clip, pos);
        }
    }
}

auto RenderingSystem::update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void {
    position = Position::Handle{};
    rendering = Rendering::Handle{};
    int cursorX, cursorY;
    SDL_GetMouseState(&cursorX, &cursorY);
    renderer.clear();
    SDL_Rect bounds{0, 0, 800, 600};
    SDL_Rect clip{0, 0, TILE_SIZE, TILE_SIZE}, pos{0, 0, TILE_SIZE, TILE_SIZE};
    renderer.copy(background, bounds, bounds);
    for (auto entity : es.entities_with_components(position, rendering)) {
        drawEntity(entity);
    }
    pos.x = cursorX - (cursorX % TILE_SIZE);
    pos.y = cursorY - (cursorY % TILE_SIZE);
    renderer.copy(shapes[CURSOR], clip, pos);
    renderer.present();
}
