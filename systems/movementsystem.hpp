#ifndef MOVEMENTSYSTEM_HPP
#define MOVEMENTSYSTEM_HPP

#include "entityx/entityx.h"
#include "core/gamestage.hpp"
#include <vector>

class MovementSystem : public entityx::System<MovementSystem> {
public:
    MovementSystem(GameStage *stage);
    auto update(entityx::EntityManager &es, entityx::EventManager &events, double dt) -> void override;
private:
    GameStage *stage; // back pointer (weak)
};

#endif // MOVEMENTSYSTEM_HPP
