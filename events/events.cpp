#include "events.hpp"

MovementOrdered::MovementOrdered(Position goal) : goal(goal) {

}

auto MovementOrdered::getGoal() const -> Position {
    return goal;
}

auto MovementOrdered::setGoal(const Position &value) -> void {
    goal = value;
}

TargetSelected::TargetSelected(Position target) : target(target) {}

auto TargetSelected::getTarget() const -> Position {
    return target;
}

auto TargetSelected::setTarget(const Position &value) -> void {
    target = value;
}
