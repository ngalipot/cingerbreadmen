#include "components/position.hpp"
#include "entityx/entityx.h"

class EscapeOrdered : public entityx::Event<EscapeOrdered> {};

class MovementOrdered : public entityx::Event<MovementOrdered> {
public:
  MovementOrdered(Position goal);
  auto getGoal() const -> Position ;
  auto setGoal(const Position &value) -> void;
private:
  Position goal;
};

class TargetSelected : public entityx::Event<TargetSelected> {
public:
  TargetSelected(Position target);
  auto getTarget() const -> Position;
  auto setTarget(const Position &value) -> void;
private:
  Position target;
};
