#ifndef MOVEMENT_HPP
#define MOVEMENT_HPP

#include "position.hpp"
#include <vector>

class Movement : public entityx::Component<Movement> {
public:
    static constexpr double BASE_PERIOD = 1.0;
    enum class Type { EARTH, WATER, AIR };
    Movement(double period = BASE_PERIOD, Type type = Type::EARTH,
             std::vector<Position> moves = std::vector<Position>(), bool loop = false);
    auto getPeriod() const -> double;
    auto setPeriod(double value) -> void;
    auto getDelta() const -> double;
    auto getType() const -> Type;
    auto setType(const Type &value) -> void;
    auto getTarget() const -> Position;
    auto nextMove() const -> Position;
    auto nextNextMove() const -> Position;
    auto hasNextMove() const -> bool;
    auto hasNextNextMove() const -> bool;
    auto setMoves(const std::vector<Position> &value) -> void;
    auto moveDuring(double seconds) -> void;
private:
    double timeSinceLastMove;
    double period;
    Type type;
    std::vector<Position> moves;
    bool loop;
    std::vector<Position>::reverse_iterator moveIterator;
};

#endif // MOVEMENT_HPP
