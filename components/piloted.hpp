#ifndef PILOTED_HPP
#define PILOTED_HPP

#include "entityx/entityx.h"

class Piloted : public entityx::Component<Piloted>
{
public:
    Piloted();
    bool isSelected() const;
    void setSelected(bool selected);

private:
    bool selected;
};

#endif // PILOTED_HPP
