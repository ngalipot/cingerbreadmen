#include "collectible.hpp"

Collectible::Collectible(double effectDuration, std::function<void (entityx::Entity &)> effect) :
    timeSinceCollect(0.0), effectDuration(effectDuration), effect(effect)
{

}

double Collectible::getEffectDuration() const
{
    return effectDuration;
}

void Collectible::setEffectDuration(double value)
{
    effectDuration = value;
}
std::function<void (entityx::Entity &)> Collectible::getEffect() const
{
    return effect;
}

void Collectible::setEffect(const std::function<void (entityx::Entity &)> &value)
{
    effect = value;
}

double Collectible::getTimeSinceLastCollect()
{
    return timeSinceCollect;
}

void Collectible::consumeDuring(double seconds)
{
    timeSinceCollect += seconds;
}


