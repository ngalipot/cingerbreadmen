#ifndef HEALTH_HPP
#define HEALTH_HPP

#include <entityx/entityx.h>

class Health : public entityx::Component<Health>
{
public:
    Health();
    int getHealth() const;
    void setHealth(int value);

    int getMaxHealth() const;
    void setMaxHealth(int value);

    int getArmor() const;
    void setArmor(int value);

private:
    int health, maxHealth, armor;
};

#endif // HEALTH_HPP
