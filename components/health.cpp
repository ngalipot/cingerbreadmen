#include "health.hpp"

Health::Health() : health(100), maxHealth(100)
{
}
int Health::getHealth() const
{
    return health;
}

void Health::setHealth(int value)
{
    health = value;
}
int Health::getMaxHealth() const
{
    return maxHealth;
}

void Health::setMaxHealth(int value)
{
    maxHealth = value;
}
int Health::getArmor() const
{
    return armor;
}

void Health::setArmor(int value)
{
    armor = value;
}



