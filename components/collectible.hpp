#ifndef COLLECTIBLE_HPP
#define COLLECTIBLE_HPP

#include <entityx/entityx.h>
#include <functional>

class Collectible : public entityx::Component<Collectible>
{
public:
    Collectible(double effectDuration = 0.0, std::function<void (entityx::Entity&)> effect = std::function<void (entityx::Entity&)>());
    double getEffectDuration() const;
    void setEffectDuration(double value);

    std::function<void (entityx::Entity &)> getEffect() const;
    void setEffect(const std::function<void (entityx::Entity &)> &value);

    double getTimeSinceLastCollect();
    void consumeDuring(double seconds);
private:
    double timeSinceCollect;
    double effectDuration;
    std::function<void (entityx::Entity&)> effect;
};

#endif // COLLECTIBLE_HPP
