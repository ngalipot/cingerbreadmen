#include "movement.hpp"

Movement::Movement(double period, Type type, std::vector<Position> moves, bool loop) :
    timeSinceLastMove(0.0),
    period(period),
    type(type),
    moves(moves),
    moveIterator(moves.rbegin()),
    loop(loop) {}

auto Movement::getPeriod() const -> double
{
    return period;
}

auto Movement::setPeriod(double value) -> void
{
    period = value;
}

auto Movement::getDelta() const -> double {
    return timeSinceLastMove / period;
}

auto Movement::getType() const -> Movement::Type
{
    return type;
}

auto Movement::setType(const Type &value) -> void
{
    type = value;
}

auto Movement::nextMove() const -> Position {
    return *moveIterator;
}

auto Movement::nextNextMove() const -> Position {
    return *(moveIterator + 1);
}

auto Movement::hasNextMove() const -> bool {
    return moveIterator != moves.rend();
}

auto Movement::hasNextNextMove() const -> bool {
    return hasNextMove() && (moveIterator + 1) != moves.rend();
}

auto Movement::getTarget() const -> Position {
    return moves.front();
}

auto Movement::setMoves(const std::vector<Position> &value) -> void
{
    moves = value;
    timeSinceLastMove = 0.0;
    moveIterator = moves.rbegin();
}

auto Movement::moveDuring(double seconds) -> void {
    if (moveIterator != moves.rend()) {
        timeSinceLastMove += seconds;
        if (timeSinceLastMove >= period) {
            timeSinceLastMove -= period;
            ++moveIterator;
            if (loop && moveIterator == moves.rend()) {
                moveIterator = moves.rbegin();
            }
        }
    }
}



