#ifndef POSITION_HPP
#define POSITION_HPP

#include <entityx/entityx.h>
#include <ostream>

class Position : public entityx::Component<Position> {
public:
    Position(int x = 0, int y = 0);
    int getX() const;
    int getY() const;
    void setX(int x);
    void setY(int y);
    void set(const Position &other);
    bool operator== (const Position &other) const;
    bool operator!= (const Position &other) const;
private:
    int x, y;
};

std::ostream& operator <<(std::ostream& stream, const Position& position);

namespace std {
  template <> struct hash<Position>
  {
    size_t operator()(const Position& p) const
    {
        int hash = 7;
        hash = 71 * hash + p.getX();
        hash = 71 * hash + p.getY();
        return hash;
    }
  };
}

#endif // POSITION_HPP
