#include "position.hpp"

Position::Position(int x, int y) : x(x), y(y) {}

int Position::getX() const {
    return x;
}

int Position::getY() const {
    return y;
}

void Position::setX(int x) {
    this->x = x;
}

void Position::setY(int y) {
    this->y = y;
}

void Position::set(const Position &other) {
    x = other.x;
    y = other.y;
}

bool Position::operator== (const Position &other) const {
    return (x == other.x) && (y == other.y);
}

bool Position::operator!= (const Position &other) const {
    return (x != other.x) || (y != other.y);
}


std::ostream& operator <<(std::ostream& stream, const Position& position)
{
    stream << "Position(" << position.getX() << ", " << position.getY() << ")";
    return stream;
}
