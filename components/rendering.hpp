#ifndef RENDERING_HPP
#define RENDERING_HPP

#include <entityx/entityx.h>

class Rendering : public entityx::Component<Rendering>
{
public:
    Rendering();
};

#endif // RENDERING_HPP
