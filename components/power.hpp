#ifndef POWER_HPP
#define POWER_HPP

#include <entityx/entityx.h>

class Power : public entityx::Component<Power>
{
public:
    Power();
    int getPower() const;
    void setPower(int value);

    int getRecovery() const;
    void setRecovery(int value);

private:
    int power, recovery;
};

#endif // POWER_HPP
