#include "piloted.hpp"

Piloted::Piloted() : selected(false)
{
}
bool Piloted::isSelected() const
{
    return selected;
}

void Piloted::setSelected(bool selected) {
    this->selected = selected;
}
