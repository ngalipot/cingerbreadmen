#include "ai/pathfinding.hpp"
#include "components/position.hpp"
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

auto displayMap(bool tiles[], size_t width, size_t height, const std::vector<Position> &path) -> void;

auto main() -> int {
    bool tiles[] = {
        true, true,  false, true,  true,   true,   true,   true,  true,  true, true,
        true, true,  false, true,  true,   true,   true,   true,  true,  true, true,
        true, true,  false, true,  false,  false,  true,   false, false, true, true,
        true, true,  false, true,  true,   true,   true,   true,  true,  true, true,
        true, true,  false, true,  true,   true,   true,   true,  false, true, true,
        true, true,  true,  true,  true,   true,   true,   true,  false, true, true,
        true, true,  true,  true,  true,   true,   true,   true,  false, true, true,
        true, false, false, false, false,  false,  false,  false, false, true, true,
        true, true,  false, true,  true,   true,   false,  true,  true,  true, true,
        true, true,  true,  true,  false,  true,   true,   true,  false, true, true,
    };
    const auto width = 11;
    const auto height = 10;
    auto isWalkable = [&](const Position &pos) {
        return pos.getX() < width && pos.getY() < height && tiles[pos.getX() + width * pos.getY()];
    };
    auto path = pathfinding::shortestPathBetween(Position(0, 0), Position(10, 9), pathfinding::manhattanDistance, isWalkable);
    displayMap(tiles, width, height, path);
}

auto displayMap(bool tiles[], size_t width, size_t height, const std::vector<Position> &path) -> void {
    for (auto j = 0; j < height; j++) {
        for (auto i = 0; i < width; i++) {
            if (find(begin(path), end(path), Position(i, j)) != end(path)) {
                cout << " O";
            } else if (tiles[i + width * j]) {
                cout << " .";
            } else {
                cout << " X";
            }
        }
        cout << endl;
    }
}
