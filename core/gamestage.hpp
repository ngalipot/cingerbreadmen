#ifndef GAMESTAGE_HPP
#define GAMESTAGE_HPP

#include "entityx/entityx.h"
#include "Tmx.h"

class GameStage : public entityx::EntityX {
public:
    GameStage();
    auto tileIdAt(int x, int y) -> unsigned int;
    auto width() -> int;
    auto height() -> int;
    auto update(double dt) -> void;
    auto isOver() -> bool;
    auto setWalkableAt(int type, int x, int y, bool walkable) -> void;
    auto isWalkableAt(int type, int x, int y) -> bool;
private:
    Tmx::Map map;
    std::vector<bool> obstacleMask[3];
    const Tmx::TileLayer *baseLayer;
    entityx::Entity grunt;
};

#endif // GAMESTAGE_HPP
