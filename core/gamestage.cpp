#include "systems/pilotedsystem.hpp"
#include "systems/inputsystem.hpp"
#include "systems/movementsystem.hpp"
#include "systems/renderingsystem.hpp"
#include "systems/healthsystem.hpp"
#include "components/rendering.hpp"
#include "components/position.hpp"
#include "components/health.hpp"
#include "components/movement.hpp"
#include "components/piloted.hpp"
#include "gamestage.hpp"
#include <iostream>

using namespace std;

GameStage::GameStage() {
    map.ParseFile("img/christmas.tmx");
    if (map.HasError()) {
        cerr << "Error loading map: " << map.GetErrorText() << endl;
    } else {
        baseLayer = dynamic_cast<decltype(baseLayer)>(map.GetLayer(0));
        for (int i = 0; i < 3; i++) {
            obstacleMask[i] = vector<bool>(baseLayer->GetWidth() * baseLayer->GetHeight());
        }
    }
    systems.add<MovementSystem>(this);
    systems.add<RenderingSystem>(this);
    systems.add<InputSystem>();
    systems.add<PilotedSystem>(this);
    systems.add<HealthSystem>(this);
    systems.configure();

    grunt = entities.create();
    grunt.assign<Rendering>();
    grunt.assign<Position>(1, 1);
    grunt.assign<Piloted>();
    grunt.assign<Health>();
    grunt.assign<Movement>(0.7, Movement::Type::EARTH, vector<Position>());
}

auto GameStage::setWalkableAt(int type, int x, int y, bool walkable) -> void {
    obstacleMask[type][x + width() * y] = walkable;
}

auto GameStage::isWalkableAt(int type, int x, int y) -> bool {
    return obstacleMask[type][x + width() * y];
}

auto GameStage::tileIdAt(int x, int y) -> unsigned int {
    return baseLayer->GetTileId(x, y);
}

auto GameStage::width() -> int {
    return baseLayer->GetWidth();
}

auto GameStage::height() -> int {
    return baseLayer->GetHeight();
}

auto GameStage::isOver() -> bool {
    return systems.system<InputSystem>()->emergencyExit();
}

auto GameStage::update(double dt) -> void {
    systems.update<MovementSystem>(dt);
    systems.update<RenderingSystem>(dt);
    systems.update<InputSystem>(dt);
    systems.update<PilotedSystem>(dt);
    systems.update<HealthSystem>(dt);
}
