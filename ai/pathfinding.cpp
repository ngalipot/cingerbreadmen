#include "pathfinding.hpp"
#include <queue>
#include <unordered_map>
#include <algorithm>
#include <cmath>

namespace pathfinding {

using namespace std;

struct Node {
    Position cameFrom;
    int f, h;
    Node(Position cameFrom = Position(0, 0), int f = 0, int g = 0) :
        cameFrom(cameFrom), f(f), h(f + g) {}
};

static auto getNeighbors(const Position &p, iswalkable_t isWalkable) -> path_t {
    auto neighbors = path_t{};
    for (auto dx = p.getX() > 0 ? -1 : 0; dx <= 1; dx++) {
        for (auto dy = p.getY() > 0 ? -1 : 0; dy <= 1; dy++) {
            const auto nx = p.getX() + dx, ny = p.getY() + dy;
            if (isWalkable(Position(nx, ny))
                // crossing an obstacle in diagonal is forbidden
                && isWalkable(Position(p.getX(), ny))
                && isWalkable(Position(nx, p.getY()))) {
                neighbors.push_back(Position(nx, ny));
            }
        }
    }
    return neighbors;
}

static auto reconstructPath(unordered_map<Position, Node> &nodes, const Position &start, const Position &goal) -> path_t {
    auto path = path_t{};
    auto currentPosition = goal;
    while (currentPosition != start) {
        path.push_back(currentPosition);
        auto currentNode = nodes[currentPosition];
        if (currentPosition == currentNode.cameFrom) { // no way out !
            return vector<Position>();
        }
        currentPosition = currentNode.cameFrom;
    }
    return path;
}

auto shortestPathBetween(const Position &start, const Position &goal, distance_t distance, iswalkable_t isWalkable) -> path_t {
    auto nodes = unordered_map<Position, Node>{};
    auto comparePositions = [&](const Position &pos1, const Position &pos2) {
        if (pos1 == pos2) {
            return 0;
        }
        auto n1 = nodes[pos1];
        auto n2 = nodes[pos2];
        return n1.h > n2.h ? 1 : -1;
    };
    auto frontier = priority_queue<Position, vector<Position>, decltype(comparePositions)>{comparePositions};
    auto currentPosition = start;
    auto f = 0;
    auto g = 0;

    nodes[start] = Node{};
    frontier.push(start);
    while (!frontier.empty()) {
       currentPosition = frontier.top();
       frontier.pop();
       auto neighbors = getNeighbors(currentPosition, isWalkable);
       for (auto &neighbor : neighbors) {
           f = nodes[currentPosition].f + distance(currentPosition, neighbor);
           g = distance(neighbor, goal);
           if ((nodes.find(neighbor) == nodes.end()) || f < nodes[neighbor].f) {
               nodes[neighbor] = Node{currentPosition, f, g};
               frontier.push(neighbor);
           }
       }
    }
    return reconstructPath(nodes, start, goal);
}

auto gridDistance(const Position &p1, const Position &p2) -> uint {
    const auto dx = abs(p1.getX() - p2.getX());
    const auto dy = abs(p1.getY() - p2.getY());
    return max(dx, dy);
}

auto manhattanDistance(const Position &p1, const Position &p2) -> uint{
    const auto dx = abs(p1.getX() - p2.getX());
    const auto dy = abs(p1.getY() - p2.getY());
    return dx + dy;
}

} // namespace pathfinding
