#ifndef PATHFINDING_H
#define PATHFINDING_H

#include <vector>
#include <functional>
#include "components/position.hpp"

namespace pathfinding {

using distance_t = std::function<uint (const Position&, const Position&)>;
using iswalkable_t = std::function<bool (const Position&)>;
using path_t = std::vector<Position>;

auto shortestPathBetween(const Position &start, const Position &goal, distance_t distance, iswalkable_t isWalkable) -> path_t;

auto gridDistance(const Position &p1, const Position &p2) -> uint;
auto manhattanDistance(const Position &p1, const Position &p2) -> uint;

template<int width, int height>
auto allWalkable(const Position &position) -> bool {
    return position.getX() < width && position.getY() < height;
}
}

#endif // PATHFINDING_H
