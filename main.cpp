#include "core/gamestage.hpp"
#include <SDL.h>

using namespace std;

auto main() -> int {
    SDL_Init(SDL_INIT_VIDEO);
    GameStage stage{};
    auto lastTime = SDL_GetTicks();
    while (!stage.isOver()) {
        auto currentTime = SDL_GetTicks();
        auto elapsed = (currentTime - lastTime) / 1000.0;
        lastTime = currentTime;
        stage.update(elapsed);
    }
    SDL_Quit();
    return 0;
}
